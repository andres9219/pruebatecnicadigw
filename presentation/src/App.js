import React from 'react';
import './App.css';
import { Product } from './components/products/product';
import { Container } from 'reactstrap';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Sistema de Facturación
        </p>
      </header>
      <hr />
      <Container>
        <Product />
      </Container>
    </div>
  );
}

export default App;
