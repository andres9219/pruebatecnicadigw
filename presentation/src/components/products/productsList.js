import React, { useState, useEffect } from 'react'
import { Table } from 'reactstrap';
import { Button } from 'reactstrap';
import { ProductService } from '../../services/productService'

export const ProductList = () => {
    const [products, setProducts] = useState([]);    

    async function getProducts() {
        setProducts(await ProductService.getProducts());        
    };

    useEffect(() => {
        getProducts();
    }, []);

    return (
      <Table striped>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>REFERENCIA</th>
                        <th>DESCRIPCION</th>
                        <th>PRECIO</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        products.map((product, index) => {
                            return (
                                <tr key={index}>
                                    <th scope="row">{index + 1}</th>
                                    <td>{product.numeroDeReferencia}</td>
                                    <td>{product.descripcion}</td>
                                    <td>{product.precio}</td>
                                    <td>
                                        <Button color="default" size="sm">Ver ventas</Button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
      </Table>
    )
}