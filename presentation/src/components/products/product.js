import React, { useState } from 'react'
import { TabContent, TabPane, Nav, NavItem, NavLink, Row } from 'reactstrap';
import classnames from 'classnames'
import { ProductUnit } from './productUnit'
import { ProductList } from './productsList';

export const Product = () => {
    const [activeTab, setActiveTab] = useState('1');
    
    const toggle = tab => {
        if(activeTab !== tab) setActiveTab(tab);
    }

    return (
        <React.Fragment>
        <Nav tabs>
            <NavItem>
            <NavLink
                className={classnames({ active: activeTab === '1' })}
                onClick={() => { toggle('1') }}
            >
                Productos y Precios
            </NavLink>
            </NavItem>
            <NavItem>
            <NavLink
                className={classnames({ active: activeTab === '2' })}
                onClick={() => { toggle('2') }}
            >
                Unidades por agotar
            </NavLink>
            </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
            <TabPane tabId="1">
            <Row>
                <ProductList />
            </Row>
            </TabPane>
            <TabPane tabId="2">
            <Row>
                <ProductUnit />
            </Row>
            </TabPane>
        </TabContent>
        </React.Fragment>
    )
}