import React, { useState, useEffect } from 'react'
import { ProductService } from '../../services/productService'
import { Table } from 'reactstrap';

export const ProductUnit = () => {
    const [productsByUnit, setProductsByUnit] = useState([]);

    async function getProductsUnit() {
        setProductsByUnit(await ProductService.getProductsUnit());        
    };

    useEffect(() => {
        getProductsUnit();
    }, []);

    return (
        <Table striped>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>REFERENCIA</th>
                        <th>DESCRIPCION</th>
                        <th>PRECIO</th>
                        <th>UNIDADES</th>
                        <th>TIENDA</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        productsByUnit.map((product, index) => {
                            return (
                                <tr key={index}>
                                    <th scope="row">{index + 1}</th>
                                    <td>{product.numeroDeReferencia}</td>
                                    <td>{product.descripcion}</td>
                                    <td>{product.precio}</td>
                                    <td>{product.unidadesDisponibles}</td>
                                    <td>{product.nombreTienda}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
        </Table>
    )
}