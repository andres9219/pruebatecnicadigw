import React from 'react'

const baseRuteProduct = 'https://localhost:44333/api/Producto/';

export const ProductService = {
     getProducts : async () => {
        try {
            let response = await fetch(`${baseRuteProduct}ObtenerProductos`);
            let objeto = await response.json();
            return objeto;
        } catch (error) {
            console.log('ocurrio un error')
        }        
    },

    getProductsUnit : async () => {
        try {
            let response = await fetch(`${baseRuteProduct}ObtenerProductosPorUnidades`);
            let objeto = await response.json();
            return objeto;
        } catch (error) {
            console.log('ocurrio un error')
        }
    }
}