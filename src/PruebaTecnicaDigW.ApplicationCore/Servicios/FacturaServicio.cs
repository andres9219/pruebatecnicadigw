﻿using AutoMapper;
using PruebaTecnicaDigW.ApplicationCore.Dtos;
using PruebaTecnicaDigW.ApplicationCore.Servicios.IServicios;
using PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.ApplicationCore.Servicios
{
    public class FacturaServicio : IFacturaServicio
    {
        /// <summary>
        /// Interfaz de auto mapper.
        /// </summary>
        private readonly IMapper _Mapper;

        /// <summary>
        /// Repositorio de facturas.
        /// </summary>
        private readonly IFacturaRepositorio _FacturaRepositorio;

        public FacturaServicio(IMapper mapper, IFacturaRepositorio facturaRepositorio)
        {
            _Mapper = mapper;
            _FacturaRepositorio = facturaRepositorio;
        }

        public async Task<IReadOnlyList<ClienteDto>> ObtenerClientesPorFechaDeCompraAsync(DateTime fechaInicial, DateTime fechaFinal)
        {
            return _Mapper.Map<IReadOnlyList<ClienteDto>>(await _FacturaRepositorio.ObtenerClientesPorFechaDeCompraAsync(fechaInicial, fechaFinal));
        }
    }
}
