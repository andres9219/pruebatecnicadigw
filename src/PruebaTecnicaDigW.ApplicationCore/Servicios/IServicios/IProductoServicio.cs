﻿using PruebaTecnicaDigW.ApplicationCore.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.ApplicationCore.Servicios.IServicios
{
    public interface IProductoServicio
    {
        Task<IReadOnlyList<ProductoDto>> ObtenerTodosLosProductosAsync();

        Task<IReadOnlyList<ProductoPorTiendaDto>> ObtenerProductosEscasosAsync();
    }
}
