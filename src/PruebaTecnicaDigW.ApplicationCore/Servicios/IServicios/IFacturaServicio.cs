﻿using PruebaTecnicaDigW.ApplicationCore.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.ApplicationCore.Servicios.IServicios
{
    public interface IFacturaServicio
    {
        Task<IReadOnlyList<ClienteDto>> ObtenerClientesPorFechaDeCompraAsync(DateTime fechaInicial, DateTime fechaFinal);
    }
}
