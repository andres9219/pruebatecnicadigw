﻿using AutoMapper;
using PruebaTecnicaDigW.ApplicationCore.Dtos;
using PruebaTecnicaDigW.ApplicationCore.Servicios.IServicios;
using PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.ApplicationCore.Servicios
{
    public class ProductoServicio : IProductoServicio
    {
        /// <summary>
        /// Interfaz de auto mapper.
        /// </summary>
        private readonly IMapper _Mapper;

        /// <summary>
        /// Repositorio de productos.
        /// </summary>
        private readonly IProductoRepositorio _ProductoRepositorio;

        /// <summary>
        /// Repositorio de productos por tienda.
        /// </summary>
        private readonly IProductoTiendaRepositorio _ProTiendaRepositorio;

        public ProductoServicio(IMapper mapper, IProductoRepositorio productoRepositorio, IProductoTiendaRepositorio proTiendaRepositorio)
        {
            _Mapper = mapper;
            _ProductoRepositorio = productoRepositorio;
            _ProTiendaRepositorio = proTiendaRepositorio;
        }

        public async Task<IReadOnlyList<ProductoDto>> ObtenerTodosLosProductosAsync()
        {
            return _Mapper.Map<IReadOnlyList<ProductoDto>>(await _ProductoRepositorio.ObtenerTodosLosProductosAsync());
        }

        public async Task<IReadOnlyList<ProductoPorTiendaDto>> ObtenerProductosEscasosAsync()
        {
            return _Mapper.Map<IReadOnlyList<ProductoPorTiendaDto>>(await _ProTiendaRepositorio.ObtenerProductosEscasosAsync());
        }
    }
}
