﻿using System;

namespace PruebaTecnicaDigW.ApplicationCore.Dtos
{
    public class ClienteDto
    {
        public string Nombres { get; set; }

        public DateTime FechaDeNacimiento { get; set; }

        public string Telefono { get; set; }

        public string NumeroDocumento { get; set; }
    }
}
