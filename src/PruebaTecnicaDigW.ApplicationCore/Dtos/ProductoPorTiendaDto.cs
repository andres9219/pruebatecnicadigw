﻿using System;
using System.Collections.Generic;

namespace PruebaTecnicaDigW.ApplicationCore.Dtos
{
    public class ProductoPorTiendaDto : ProductoDto
    {
        public int UnidadesDisponibles { get; set; }

        public string NombreTienda { get; set; }

        public string DireccionTienda { get; set; }
    }
}
