﻿namespace PruebaTecnicaDigW.ApplicationCore.Dtos
{
    public class ProductoDto
    {
        public string Descripcion { get; set; }

        public string NumeroDeReferencia { get; set; }

        public decimal Precio { get; set; }
    }
}
