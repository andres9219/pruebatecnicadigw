﻿using AutoMapper;
using PruebaTecnicaDigW.ApplicationCore.Dtos;
using PruebaTecnicaDigW.Infraestruture.Entidades;

namespace PruebaTecnicaDigW.ApplicationCore.Utilidades
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Producto, ProductoDto>().ReverseMap();
            CreateMap<ProductoTienda, ProductoPorTiendaDto>().ForMember(protie => protie.Descripcion,
                                                                        protie => protie.MapFrom(pro => pro.Producto.Descripcion))
                                                             .ForMember(protie => protie.NumeroDeReferencia,
                                                                        protie => protie.MapFrom(pro => pro.Producto.NumeroDeReferencia))
                                                             .ForMember(protie => protie.Precio,
                                                                        protie => protie.MapFrom(pro => pro.Producto.Precio))
                                                             .ForMember(protie => protie.NombreTienda,
                                                                        protie => protie.MapFrom(pro => pro.Tienda.Nombre))
                                                             .ForMember(protie => protie.DireccionTienda,
                                                                        protie => protie.MapFrom(pro => pro.Tienda.Direccion))
                                                             .ReverseMap();

            CreateMap<Factura, Cliente>().ForMember(cli => cli.Direccion,
                                                    cli => cli.MapFrom(fac => fac.Cliente.Direccion))
                                         .ForMember(cli => cli.Nombres,
                                                    cli => cli.MapFrom(fac => fac.Cliente.Nombres))
                                         .ForMember(cli => cli.FechaDeNacimiento,
                                                    cli => cli.MapFrom(fac => fac.Cliente.FechaDeNacimiento))
                                         .ForMember(cli => cli.NumeroDocumento,
                                                    cli => cli.MapFrom(fac => fac.Cliente.NumeroDocumento)).ReverseMap();
        }
    }
}
