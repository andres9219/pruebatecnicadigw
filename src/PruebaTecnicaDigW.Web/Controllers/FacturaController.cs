﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PruebaTecnicaDigW.ApplicationCore.Dtos;
using PruebaTecnicaDigW.ApplicationCore.Servicios.IServicios;
using PruebaTecnicaDigW.Web.Models;

namespace PruebaTecnicaDigW.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacturaController : ControllerBase
    {
        private readonly IFacturaServicio _FacturaServicio;

        public FacturaController(IFacturaServicio facturaServicio)
        {
            _FacturaServicio = facturaServicio;
        }

        [HttpPost("ObtenerClientesPorFechas")]
        public async Task<ActionResult<IReadOnlyList<ClienteDto>>> ObtenerClientesPorFechasAsync([FromBody] FacturaModel facturaModel)
        {
            try
            {
                return Ok(await _FacturaServicio.ObtenerClientesPorFechaDeCompraAsync(facturaModel.FechaInicial, facturaModel.FechaFinal));
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}