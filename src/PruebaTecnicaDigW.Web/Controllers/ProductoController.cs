﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PruebaTecnicaDigW.ApplicationCore.Dtos;
using PruebaTecnicaDigW.ApplicationCore.Servicios.IServicios;

namespace PruebaTecnicaDigW.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private readonly IProductoServicio _ProductoServicio;

        public ProductoController(IProductoServicio productoServicio)
        {
            _ProductoServicio = productoServicio;
        }

        [HttpGet("ObtenerProductos")]
        public async Task<ActionResult<IReadOnlyList<ProductoDto>>> ObtenerProductosAsync()
        {
            try
            {
                return Ok(await _ProductoServicio.ObtenerTodosLosProductosAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("ObtenerProductosPorUnidades")]
        public async Task<ActionResult<IReadOnlyList<ProductoPorTiendaDto>>> ObtenerProductosPorUnidadesAsync()
        {
            try
            {
                return Ok(await _ProductoServicio.ObtenerProductosEscasosAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}