using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PruebaTecnicaDigW.ApplicationCore.Servicios;
using PruebaTecnicaDigW.ApplicationCore.Servicios.IServicios;
using PruebaTecnicaDigW.ApplicationCore.Utilidades;
using PruebaTecnicaDigW.Infraestruture.ContextoDeDatos;
using PruebaTecnicaDigW.Infraestruture.Repositorios;
using PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios;

namespace PruebaTecnicaDigW.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped(typeof(IRepositorioAsync<>), typeof(RepositorioBase<>));
            services.AddScoped<IProductoRepositorio, ProductoRepositorio>();
            services.AddScoped<IProductoTiendaRepositorio, ProductoTiendaRepositorio>();
            services.AddScoped<IProductoServicio, ProductoServicio>();
            services.AddDbContextPool<FacturacionDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("SqlServerDev")));
            services.AddSingleton(new AutoMapper.MapperConfiguration(a => a.AddProfile(new AutoMapperProfile())).CreateMapper());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder
               .AllowAnyOrigin()
               .AllowAnyHeader()
               .AllowAnyMethod()
            );

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
