﻿using System;

namespace PruebaTecnicaDigW.Web.Models
{
    public class FacturaModel
    {
        public DateTime FechaInicial { get; set; }

        public DateTime FechaFinal { get; set; }
    }
}
