﻿using PruebaTecnicaDigW.Infraestruture.ContextoDeDatos;
using PruebaTecnicaDigW.Infraestruture.Entidades;
using PruebaTecnicaDigW.Infraestruture.Especificaciones;
using PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.Infraestruture.Repositorios
{
    public class FacturaRepositorio : RepositorioBase<Factura>, IFacturaRepositorio
    {
        public FacturaRepositorio(FacturacionDbContext contexto) : base(contexto)
        {

        }

        public async Task<IReadOnlyList<Factura>> ObtenerClientesPorFechaDeCompraAsync(DateTime fechaInicio, DateTime fechaFinal)
        {
            var especificacion = new FacturaEspecificacion(fechaInicio, fechaFinal);
            var listado = await ListarAsync(especificacion);

            if (listado.Any())
            {
                return (IReadOnlyList<Factura>)listado.GroupBy(p => p.Cliente.NumeroDocumento).Select(p => p).ToList();
            }

            return null;
        }
    }
}
