﻿using PruebaTecnicaDigW.Infraestruture.ContextoDeDatos;
using PruebaTecnicaDigW.Infraestruture.Entidades;
using PruebaTecnicaDigW.Infraestruture.Especificaciones;
using PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.Infraestruture.Repositorios
{
    public class ProductoTiendaRepositorio : RepositorioBase<ProductoTienda>, IProductoTiendaRepositorio
    {
        public ProductoTiendaRepositorio(FacturacionDbContext contexto) : base(contexto)
        {

        }

        public async Task<IReadOnlyList<ProductoTienda>> ObtenerProductosEscasosAsync()
        {
            var especificacion = new ProductoTiendaEspecificacion();
            return await ListarAsync(especificacion);
        }
    }
}
