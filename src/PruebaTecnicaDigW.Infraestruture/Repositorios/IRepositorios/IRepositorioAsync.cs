﻿using PruebaTecnicaDigW.Infraestruture.Especificaciones;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios
{
    public interface IRepositorioAsync<T> where T : class
    {
        Task<T> ObtenerEntidadPorEspecificacionAsync(IEspecificacion<T> spec);

        Task<bool> ExisteAlgunRegistroConEspecificacionAsync(IEspecificacion<T> spec);

        Task<T> ObtenerPorIdAsync(long id);

        Task<T> ObtenerPorIdAsync(Guid id);

        Task<IReadOnlyList<T>> ListarTodoAsync();

        Task<IReadOnlyList<T>> ListarAsync(IEspecificacion<T> spec);

        Task<T> AgregarAsync(T entidad);

        Task ActualizarAsync(T entidad);

        Task EliminarAsync(T entidad);

        Task<int> CantidadDeRegistrosAsync(IEspecificacion<T> spec);
    }
}
