﻿using PruebaTecnicaDigW.Infraestruture.Entidades;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios
{
    public interface IProductoRepositorio
    {
        Task<IReadOnlyList<Producto>> ObtenerTodosLosProductosAsync();
    }
}
