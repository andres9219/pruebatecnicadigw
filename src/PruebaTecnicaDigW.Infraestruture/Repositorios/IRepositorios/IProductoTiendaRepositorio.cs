﻿using PruebaTecnicaDigW.Infraestruture.Entidades;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios
{
    public interface IProductoTiendaRepositorio
    {
        Task<IReadOnlyList<ProductoTienda>> ObtenerProductosEscasosAsync();
    }
}
