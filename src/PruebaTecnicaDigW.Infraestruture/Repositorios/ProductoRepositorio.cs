﻿using PruebaTecnicaDigW.Infraestruture.ContextoDeDatos;
using PruebaTecnicaDigW.Infraestruture.Entidades;
using PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.Infraestruture.Repositorios
{
    public class ProductoRepositorio : RepositorioBase<Producto>, IProductoRepositorio
    {
        public ProductoRepositorio(FacturacionDbContext contexto) : base(contexto)
        {

        }

        public async Task<IReadOnlyList<Producto>> ObtenerTodosLosProductosAsync()
        {
            return await ListarTodoAsync();
        }
    }
}
