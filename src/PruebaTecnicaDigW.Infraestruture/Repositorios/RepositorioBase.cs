﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnicaDigW.Infraestruture.ContextoDeDatos;
using PruebaTecnicaDigW.Infraestruture.Especificaciones;
using PruebaTecnicaDigW.Infraestruture.Repositorios.IRepositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaTecnicaDigW.Infraestruture.Repositorios
{
    public class RepositorioBase<T> : IRepositorioAsync<T> where T : class
    {
        protected readonly FacturacionDbContext _Contexto;

        public RepositorioBase(FacturacionDbContext facturacionDbContext)
        {
            _Contexto = facturacionDbContext;
        }

        #region Implementacion de IRepositorioAsync

        public virtual async Task<bool> ExisteAlgunRegistroConEspecificacionAsync(IEspecificacion<T> spec)
        {
            return await AplicarEspecificacion(spec).AnyAsync();
        }

        public virtual async Task<T> ObtenerEntidadPorEspecificacionAsync(IEspecificacion<T> spec)
        {
            return await AplicarEspecificacion(spec).FirstOrDefaultAsync();
        }

        public virtual async Task<int> CantidadDeRegistrosAsync(IEspecificacion<T> spec)
        {
            return await AplicarEspecificacion(spec).CountAsync();
        }

        public virtual async Task<T> AgregarAsync(T entidad)
        {
            await _Contexto.AddAsync(entidad);
            await _Contexto.SaveChangesAsync();
            return entidad;
        }

        public virtual async Task ActualizarAsync(T entidad)
        {
            _Contexto.Entry(entidad).State = EntityState.Modified;
            await _Contexto.SaveChangesAsync();
        }

        public virtual async Task<T> ObtenerPorIdAsync(long id)
        {
            return await _Contexto.Set<T>().FindAsync(id);
        }

        public virtual async Task<T> ObtenerPorIdAsync(Guid id)
        {
            return await _Contexto.Set<T>().FindAsync(id);
        }

        public virtual async Task EliminarAsync(T entidad)
        {
            _Contexto.Set<T>().Remove(entidad);
            await _Contexto.SaveChangesAsync();
        }

        public virtual async Task<IReadOnlyList<T>> ListarAsync(IEspecificacion<T> spec)
        {
            return await AplicarEspecificacion(spec).ToListAsync();
        }

        public virtual async Task<IReadOnlyList<T>> ListarTodoAsync()
        {
            return await _Contexto.Set<T>().AsNoTracking().ToListAsync();
        }

        #endregion

        /// <summary>
        /// Llama metodo de clase padre con el fin de aplicar la especificacion de la consulta deseada
        /// </summary>
        /// <param name="spec">Especificacion requerida</param>
        /// <returns></returns>
        private IQueryable<T> AplicarEspecificacion(IEspecificacion<T> spec)
        {
            return EspecificacionEvaluador<T>.ObtenerConsulta(_Contexto.Set<T>().AsNoTracking(), spec);
        }
    }
}
