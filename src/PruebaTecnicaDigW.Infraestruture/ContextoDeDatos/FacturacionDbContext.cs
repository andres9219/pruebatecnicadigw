﻿using Microsoft.EntityFrameworkCore;
using PruebaTecnicaDigW.Infraestruture.Entidades;

namespace PruebaTecnicaDigW.Infraestruture.ContextoDeDatos
{
    /// <summary>
    /// Contexto de datos de la aplicacion.
    /// </summary>
    public class FacturacionDbContext : DbContext
    {
        #region Entidades DbSet

        public virtual DbSet<Producto> Productos { get; set; }

        public virtual DbSet<ProductoTienda> ProductosTienda { get; set; }

        public virtual DbSet<Tienda> Tiendas { get; set; }

        public virtual DbSet<Cliente> Clientes { get; set; }

        public virtual DbSet<Colaborador> Colaboradores { get; set; }

        public virtual DbSet<Factura> Facturas { get; set; }

        #endregion

        #region Constructores

        public FacturacionDbContext(DbContextOptions<FacturacionDbContext> options) : base(options)
        {

        }

        #endregion

        #region Metodos de configuracion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>().HasIndex(cli => cli.NumeroDocumento).IsUnique();
            modelBuilder.Entity<Producto>().HasIndex(pro => pro.NumeroDeReferencia).IsUnique();
            modelBuilder.Entity<Colaborador>().HasIndex(col => col.NumeroDocumento).IsUnique();
            modelBuilder.Entity<ProductoTienda>().HasIndex(pro => new { pro.ProductoId, pro.TiendaId }).IsUnique();

            modelBuilder.Entity<Colaborador>().HasMany(col => col.Facturas)
                                              .WithOne(fact => fact.Colaborador)
                                              .HasForeignKey(col => col.FacturaId)
                                              .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Cliente>().HasMany(cli => cli.Facturas)
                                          .WithOne(fact => fact.Cliente)
                                          .HasForeignKey(col => col.ClienteId)
                                          .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Producto>().HasMany(pro => pro.Facturas)
                                           .WithOne(fact => fact.Producto)
                                           .HasForeignKey(col => col.ProductoId)
                                           .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Producto>().HasMany(pro => pro.ProductosTienda)
                                           .WithOne(proTie => proTie.Producto)
                                           .HasForeignKey(col => col.ProductoId)
                                           .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Tienda>().HasMany(tie => tie.ProductosTienda)
                                         .WithOne(proTie => proTie.Tienda)
                                         .HasForeignKey(col => col.TiendaId)
                                         .OnDelete(DeleteBehavior.Cascade);

            //base.OnModelCreating(modelBuilder);
        }

        #endregion
    }
}
