﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PruebaTecnicaDigW.Infraestruture.Migrations
{
    public partial class AgregadoDeColumnaCantidadDeUnidadesDeProductosPorTienda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UnidadesDisponibles",
                table: "ProductosTienda",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UnidadesDisponibles",
                table: "ProductosTienda");
        }
    }
}
