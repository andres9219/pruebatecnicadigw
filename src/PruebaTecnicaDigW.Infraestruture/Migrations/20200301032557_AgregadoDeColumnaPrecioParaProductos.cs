﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PruebaTecnicaDigW.Infraestruture.Migrations
{
    public partial class AgregadoDeColumnaPrecioParaProductos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Precio",
                table: "Productos",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Precio",
                table: "Productos");
        }
    }
}
