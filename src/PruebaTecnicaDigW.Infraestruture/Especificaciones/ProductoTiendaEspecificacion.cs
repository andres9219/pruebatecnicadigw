﻿using PruebaTecnicaDigW.Infraestruture.Entidades;

namespace PruebaTecnicaDigW.Infraestruture.Especificaciones
{
    public class ProductoTiendaEspecificacion : EspecificacionBase<ProductoTienda>
    {
        public ProductoTiendaEspecificacion() : base(producto => producto.UnidadesDisponibles <= 5)
        {
            AgregarInclusion(producto => producto.Producto);
            AgregarInclusion(producto => producto.Tienda);
        }
    }
}
