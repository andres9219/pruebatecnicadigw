﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PruebaTecnicaDigW.Infraestruture.Especificaciones
{
    public interface IEspecificacion<T>
    {
        Expression<Func<T, bool>> Criterio { get; }

        List<Expression<Func<T, bool>>> CriteriosOpcionales { get; }

        List<Expression<Func<T, object>>> Incluir { get; }

        List<string> IncluirComoCadena { get; }

        Expression<Func<T, object>> OrdenarPor { get; }

        Expression<Func<T, object>> OrdenarDescendentePor { get; }

        #region Variable de paginacion

        int Cantidad { get; }

        int Iniciar { get; }

        bool EsPaginacionHabilitada { get; }

        #endregion        
    }
}
