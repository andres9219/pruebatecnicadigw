﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace PruebaTecnicaDigW.Infraestruture.Especificaciones
{
    public class EspecificacionEvaluador<T> where T : class
    {
        /// <summary>
        /// Realiza la asignacion a la consulta de las especificaciones dadas
        /// </summary>
        /// <param name="consultaEntrante">consulta sql a ejecutarse</param>
        /// <param name="spec">Especificaciones dadas para la consulta</param>
        /// <returns>consulta sql con las especificaciones inyectadas</returns>
        public static IQueryable<T> ObtenerConsulta(IQueryable<T> consultaEntrante, IEspecificacion<T> spec)
        {
            var consulta = consultaEntrante;

            if (spec.Criterio != null)
            {
                consulta = consulta.Where(spec.Criterio);
            }

            if (spec.CriteriosOpcionales.Count > 0)
            {
                foreach (var item in spec.CriteriosOpcionales)
                {
                    consulta = consulta.Where(item);
                }
            }

            consulta = spec.Incluir.Aggregate(consulta, (current, inclusion) => current.Include(inclusion));
            consulta = spec.IncluirComoCadena.Aggregate(consulta, (current, inclusion) => current.Include(inclusion));

            if (spec.OrdenarPor != null)
            {
                consulta = consulta.OrderBy(spec.OrdenarPor);
            }
            else if (spec.OrdenarDescendentePor != null)
            {
                consulta = consulta.OrderByDescending(spec.OrdenarDescendentePor);
            }

            if (spec.EsPaginacionHabilitada)
            {
                consulta = consulta.Skip(spec.Iniciar).Take(spec.Cantidad);
            }

            return consulta;
        }
    }
}
