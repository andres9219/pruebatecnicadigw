﻿using PruebaTecnicaDigW.Infraestruture.Entidades;
using System;

namespace PruebaTecnicaDigW.Infraestruture.Especificaciones
{
    public class FacturaEspecificacion : EspecificacionBase<Factura>
    {
        public FacturaEspecificacion(DateTime fechaInicio, DateTime fechaFinal) : base(fact => fact.FechaDeExpedicion <= fechaFinal && fact.FechaDeExpedicion >= fechaInicio)
        {
            AgregarInclusion(fact => fact.Cliente);
            AgregarCriterioOpcional(cliente => (DateTime.Today.AddTicks(-cliente.FechaDeExpedicion.Value.Ticks).Year - 1) <= 35);
        }
    }
}
