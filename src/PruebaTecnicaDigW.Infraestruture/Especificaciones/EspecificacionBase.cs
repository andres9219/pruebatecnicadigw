﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PruebaTecnicaDigW.Infraestruture.Especificaciones
{
    public abstract class EspecificacionBase<T> : IEspecificacion<T>
    {
        #region Propiedades

        public int Cantidad { get; private set; }

        public int Iniciar { get; private set; }

        public bool EsPaginacionHabilitada { get; private set; } = false;

        public Expression<Func<T, bool>> Criterio { get; private set; }

        public List<Expression<Func<T, bool>>> CriteriosOpcionales { get; } = new List<Expression<Func<T, bool>>>();

        public List<Expression<Func<T, object>>> Incluir { get; } = new List<Expression<Func<T, object>>>();

        public List<string> IncluirComoCadena { get; } = new List<string>();

        public Expression<Func<T, object>> OrdenarPor { get; private set; }

        public Expression<Func<T, object>> OrdenarDescendentePor { get; private set; }

        #endregion

        #region Constructores

        protected EspecificacionBase(Expression<Func<T, bool>> criterio)
        {
            Criterio = criterio;
        }

        #endregion

        #region Implementacion de metodos accesores

        protected virtual void AgregarCriterioOpcional(Expression<Func<T, bool>> criterioOptional)
        {
            CriteriosOpcionales.Add(criterioOptional);
        }

        protected virtual void AgregarInclusion(Expression<Func<T, object>> expresionDeInclusion)
        {
            Incluir.Add(expresionDeInclusion);
        }

        protected virtual void AgregarInclusion(string expresionDeInclusion)
        {
            IncluirComoCadena.Add(expresionDeInclusion);
        }

        protected virtual void AplicarPaginacion(int cantidad, int inicio)
        {
            Cantidad = cantidad;
            Iniciar = inicio;
            EsPaginacionHabilitada = true;
        }

        /// <summary>
        /// Asigna la expresion de ordenamiento recibida como parametro
        /// </summary>
        /// <param name="expresionOrdenamiento"></param>
        protected virtual void AplicarOrdenamientoPorPropiedad(Expression<Func<T, object>> expresionOrdenamiento)
        {
            OrdenarPor = expresionOrdenamiento;
        }

        /// <summary>
        /// Asigna la expresion de ordenamiento recibida como parametro
        /// </summary>
        /// <param name="expresionOrdenamiento"></param>
        protected virtual void AplicarOrdenamientoDescPorPropiedad(Expression<Func<T, object>> expresionOrdenamiento)
        {
            OrdenarDescendentePor = expresionOrdenamiento;
        }

        #endregion
    }
}
