﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaTecnicaDigW.Infraestruture.Entidades
{
    [Table("Tiendas")]
    public class Tienda
    {
        [Key]
        public long TiendaId { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public string Direccion { get; set; }

        #region Entidades relacionadas
        public virtual ICollection<ProductoTienda> ProductosTienda { get; set; }

        #endregion
    }
}
