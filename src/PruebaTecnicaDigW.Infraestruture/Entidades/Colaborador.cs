﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaTecnicaDigW.Infraestruture.Entidades
{
    [Table("Colaboradores")]
    public class Colaborador
    {
        [Key]
        public Guid ColaboradorId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Nombres { get; set; }

        [Required]
        public DateTime? FechaDeNacimiento { get; set; }

        [Required]
        public string Direccion { get; set; }

        [Required]
        public string Telefono { get; set; }

        [Required]
        public string NumeroDocumento { get; set; }

        #region Entidades relacionadas

        public virtual ICollection<Factura> Facturas { get; set; }

        #endregion
    }
}
