﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaTecnicaDigW.Infraestruture.Entidades
{
    [Table("Facturas")]
    public class Factura
    {
        [Key]
        public Guid FacturaId { get; set; }

        public string Descripcion { get; set; }

        public DateTime? FechaDeExpedicion { get; set; }

        public Guid ColaboradorId { get; set; }

        public Guid ClienteId { get; set; }

        public long ProductoId { get; set; }

        #region Entidades relacionadas

        public virtual Colaborador Colaborador { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual Producto Producto { get; set; }

        #endregion
    }
}
