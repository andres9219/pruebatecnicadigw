﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaTecnicaDigW.Infraestruture.Entidades
{
    [Table("ProductosTienda")]
    public class ProductoTienda
    {
        [Key]
        public long ProductoTiendaId { get; set; }

        public int UnidadesDisponibles { get; set; }

        public long ProductoId { get; set; }

        public long TiendaId { get; set; }

        #region Entidades relacionadas

        public virtual Tienda Tienda { get; set; }

        public virtual Producto Producto { get; set; }

        #endregion
    }
}
