﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaTecnicaDigW.Infraestruture.Entidades
{
    [Table("Productos")]
    public class Producto
    {
        [Key]
        public long ProductoId { get; set; }

        public string Descripcion { get; set; }

        public string NumeroDeReferencia { get; set; }

        public decimal Precio { get; set; }

        #region Entidades relacionadas

        public virtual ICollection<ProductoTienda> ProductosTienda { get; set; }

        public virtual ICollection<Factura> Facturas { get; set; }

        #endregion
    }
}
